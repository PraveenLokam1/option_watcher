# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# import pandas as pd
#
# driver = webdriver.Chrome(executable_path='chromedriver.exe')
# driver.get('https://worldpopulationreview.com/countries/countries-by-gdp')#put here the adress of your page
# btn = driver.find_element_by_xpath('/html/body/div[1]/div/div[1]/div[2]/div[2]/div[1]/div/div/div/div[2]/div[1]/a[2]')
# btn.click()
# df = pd.read_csv('/Users/xxx/Downloads/data.csv')
# print(df.head())
# driver.close()

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import Select
import time

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get('https://www.nseindia.com/option-chain')
time.sleep(3)
#driver.find_element_by_id('equity_optionchain_select').find_element_by_name('Select').click()
#driver.find_element_by_xpath("//select[@name='optionContract']/option[text()='BANKNIFTY']").click()
select = Select(driver.find_element_by_id('equity_optionchain_select'))

# select by visible text
#select.select_by_visible_text('BANKNIFTY')

# # select by value
select.select_by_value('BANKNIFTY')
time.sleep(3)
#select = Select(driver.find_element_by_id('expiryDateFilter'))
# arrow = driver.find_element_by_xpath('//div[@id="expiryDateFilter" and @class="col-md-2 blockDivider"]')\
#     .find_element_by_class_name('custom_select')
# arrow.click()

# select by visible text
#select.select_by_visible_text('BANKNIFTY')

select = Select(driver.find_element_by_id('expirySelect'))
# # select by value
select.select_by_value('08-Jul-2021')

download_elem = driver.find_element_by_id('downloadOCTable')\
    .find_element_by_xpath("//input[@src='/assets/images/icon-xls.svg' and @type='image']").click()
print(download_elem)

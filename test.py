import pandas as pd
from util import read_input_df

pd.set_option('display.max_columns', None)
pd.set_option('expand_frame_repr', False)

df = read_input_df('test.csv')

c_df = df[df['optionType'] == 'CE']
p_df = df[df['optionType'] == 'PE']

print(c_df)
print(p_df)

c_df['key'] = 1
p_df['key'] = 1

merge_df = pd.merge(c_df, p_df, on='key').drop(["key"], 1)

merge_df.loc[:, 'diff_from_115'] = merge_df.apply(lambda row: abs(row['absolute_difference_x'] + row['absolute_difference_y']), axis=1)
merge_df.loc[:, 'combo_diff'] = merge_df.apply(lambda row: abs(row['lastPrice_x'] - row['lastPrice_y']), axis=1)
merge_df.loc[:, 'all_diff'] = merge_df.apply(lambda row: (row['combo_diff'] + row['diff_from_115']), axis=1)
print(merge_df)

min_df = merge_df[merge_df.all_diff == merge_df.all_diff.min()]
if len(min_df.index) > 1:
    min_df = min_df[min_df.combo_diff == min_df.combo_diff.min()]
print(min_df)

final_df = min_df.drop(['diff_from_115', 'combo_diff', 'all_diff'], 1)
print(final_df)

final_cols = [col[:-2] for col in list(final_df) if col.endswith('_x')]

x_cols = [col for col in list(final_df) if col.endswith('_x')]
x_df = final_df[x_cols]

y_cols = [col for col in list(final_df) if col.endswith('_y')]
y_df = final_df[y_cols]
print(x_df)
print(y_df)

x_df.columns = final_cols
y_df.columns = final_cols
print(x_df)
print(y_df)

selected_options_df = x_df.append(y_df)
print(selected_options_df)
option_types = set(selected_options_df['optionType'])
if option_types == {'PE', 'CE'}:
    print('Both CALL and PUT options are selected for next week...')
else:
    print('Check the select_final_options method in find_required_options.py')
import pandas as pd

def write_option_data_to_csv(df: pd.DataFrame, path, header: bool = True, mode: str = 'w',):
    print(f'Writing data to CSV in path : {path}')
    df.to_csv(path, index=False, header=header, mode=mode)

def read_input_df(path):
    return pd.read_csv(path)

def mobile_alert():
    print('Alert SMS sending...')
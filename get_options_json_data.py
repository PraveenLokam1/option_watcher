import requests
import json
import pandas as pd
from option_config import url
from option_config import CONFIG, ALL_OPTION_DATA_PATH, HEADERS
from util import write_option_data_to_csv

expiry_date = CONFIG['expiry_date']

pd.set_option('display.max_columns', None)


def get_option_data():
    json_obj = requests.get(url, headers = HEADERS).json()
    print(f"Total option data count received count: {len(json_obj['records']['data'])}")

    temp_data = [option_val for option_val in json_obj['records']['data'] if option_val['expiryDate'] == expiry_date]
    next_week_data = [{**option_val['CE'], **{'optionType': 'CE'}} for option_val in temp_data] + \
                        [{**option_val['PE'], **{'optionType': 'PE'}} for option_val in temp_data]
    print(f"Extracted count of CALL and PUT options data extracted for this week {expiry_date} : {len(next_week_data)}")

    df = pd.DataFrame(next_week_data)[['optionType', 'underlyingValue', 'strikePrice', 'lastPrice']]
    return df

def extract_option_data():
    df = None
    try:
        df = get_option_data()
    except Exception:
        print('Option data extraction from NSE site failed...try after some time')
        exit()
    write_option_data_to_csv(df, ALL_OPTION_DATA_PATH)

if __name__ == "__main__":
    extract_option_data()
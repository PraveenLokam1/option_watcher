from get_options_json_data import extract_option_data
from find_required_options import process_option_data


if __name__ == "__main__":
    print('Short Strangle process started...')
    extract_option_data()
    process_option_data()

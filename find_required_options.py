import pandas as pd
from option_config import PREMIUM, ALL_OPTION_DATA_PATH, OPTION_LOG_PATH, SELECTED_WEEK_OPTIONS_PATH, PREMIUM_FALL_PERCENT
from pathlib import Path
from util import write_option_data_to_csv, mobile_alert, read_input_df

def clean_df(df: pd.DataFrame):
    df = df[((df['strikePrice'] > df['underlyingValue']) & (df['optionType'] == 'CE')) |
            ((df['strikePrice'] < df['underlyingValue']) & (df['optionType'] == 'PE'))]
    return df

def add_diff_col_df(df : pd.DataFrame):
    df.loc[:, 'absolute_difference'] = df['lastPrice'].map(lambda x : abs(x - PREMIUM))
    return df

def final_data_df(df: pd.DataFrame):
    temp_df = None
    for i in range(20):
        temp_df = df[df['absolute_difference'] <= i]
        option_types = set(temp_df['optionType'])
        if option_types == {'PE', 'CE'}:
            print('Both CALL and PUT options available...')
            break
    return temp_df

def select_final_options(df: pd.DataFrame):
    c_df = df[df['optionType'] == 'CE']
    p_df = df[df['optionType'] == 'PE']

    c_df['key'] = 1
    p_df['key'] = 1

    merge_df = pd.merge(c_df, p_df, on='key').drop(["key"], 1)

    merge_df.loc[:, 'diff_from_115'] = merge_df.apply(
        lambda row: abs(row['absolute_difference_x'] + row['absolute_difference_y']), axis=1)
    merge_df.loc[:, 'combo_diff'] = merge_df.apply(lambda row: abs(row['lastPrice_x'] - row['lastPrice_y']), axis=1)
    merge_df.loc[:, 'all_diff'] = merge_df.apply(lambda row: (row['combo_diff'] + row['diff_from_115']), axis=1)

    min_df = merge_df[merge_df.all_diff == merge_df.all_diff.min()]
    if len(min_df.index) > 1:
        min_df = min_df[min_df.combo_diff == min_df.combo_diff.min()]

    final_df = min_df.drop(['diff_from_115', 'combo_diff', 'all_diff'], 1)

    final_cols = [col[:-2] for col in list(final_df) if col.endswith('_x')]

    x_cols = [col for col in list(final_df) if col.endswith('_x')]
    x_df = final_df[x_cols]

    y_cols = [col for col in list(final_df) if col.endswith('_y')]
    y_df = final_df[y_cols]

    x_df.columns = final_cols
    y_df.columns = final_cols

    selected_options_df = x_df.append(y_df)
    option_types = set(selected_options_df['optionType'])
    if option_types == {'PE', 'CE'}:
        print('Both CALL and PUT options are selected for next week...')
    else:
        print('Check the select_final_options method in find_required_options.py')

    return selected_options_df

def generate_final_options():
    df = read_input_df(ALL_OPTION_DATA_PATH)
    cleaned_df = clean_df(df)
    diff_col_df = add_diff_col_df(cleaned_df)
    final_df = final_data_df(diff_col_df)
    pd.set_option('display.max_rows', None)
    selected_df = select_final_options(final_df)
    write_option_data_to_csv(selected_df, SELECTED_WEEK_OPTIONS_PATH)
    write_option_data_to_csv(selected_df.drop('absolute_difference', 1), OPTION_LOG_PATH)

def selected_options_exist(path_string: str):
    selected_file = Path(path_string)
    if selected_file.is_file():
        return True
    else:
        return False

def check_premium_drop(selected_options_df: pd.DataFrame, current_df: pd.DataFrame, only_sell:bool = True):
    print('Calculating current premium...')
    if only_sell:
        selected_premium = selected_options_df[selected_options_df['optionType'] == 'CE']['lastPrice'].iloc[0]  + \
                           selected_options_df[selected_options_df['optionType'] == 'PE']['lastPrice'].iloc[0]

        print(f'Selected premium : {selected_premium}')

        current_premium = current_df[current_df['optionType'] == 'CE']['lastPrice'].iloc[0]  + \
                          current_df[current_df['optionType'] == 'PE']['lastPrice'].iloc[0]

        print(f'Current premium : {current_premium}')

        fall_premium_perc = round((100 - (100 * current_premium) / selected_premium), 2)
        if (PREMIUM_FALL_PERCENT - fall_premium_perc) <= 5:
            print(f'Alerting to exit the options... as current option fall at : {fall_premium_perc}')
            mobile_alert()
        else:
            print(f'Wait for some time as current option fall at : {fall_premium_perc} %')
    else:
        print('Need logic to calculate premium of BUY and SELL options....')

def current_option_data():
    df = read_input_df(ALL_OPTION_DATA_PATH)
    selected_options_df = read_input_df(SELECTED_WEEK_OPTIONS_PATH)
    selected_options_dict = selected_options_df.set_index('optionType').T.to_dict('dict')
    call_strike_price = selected_options_dict['CE']['strikePrice']
    put_strike_price = selected_options_dict['PE']['strikePrice']
    current_df = df.loc[((df.optionType == "CE") & (df.strikePrice == call_strike_price)) |
                        (df.optionType == "PE") & (df.strikePrice == put_strike_price)]

    check_premium_drop(selected_options_df, current_df)
    write_option_data_to_csv(current_df, OPTION_LOG_PATH, mode='a', header=False)

def log_current_option_data():
    current_option_data()

def process_option_data():
    if selected_options_exist(SELECTED_WEEK_OPTIONS_PATH):
        print('Selected options exist for current week...')
        print("Logging current run's updated selected option data.... ")
        log_current_option_data()
    else:
        generate_final_options()

if __name__ == "__main__":
    process_option_data()

